Data Manipulation Language (DML) statements are used for managing data within schema objects. Some examples:

 1. SELECT - retrieve data from the a database
 2. INSERT - insert data into a table UPDATE - updates existing data within a table
 3. DELETE - deletes all records from a table, the space for the records remain
 4. MERGE - UPSERT operation (insert or update)
 5. CALL - call a PL/SQL or Java subprogram
 6. EXPLAIN PLAN - explain access path to data
 7. LOCK TABLE - control concurrency
