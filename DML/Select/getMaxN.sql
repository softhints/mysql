--get N top records
-- <=3 get all less then 4

SELECT co.id, co.category_id
FROM clash co
WHERE (
SELECT COUNT(*)
FROM clash ci
WHERE STATUS = 1 AND co.category_id = ci.category_id AND co.id < ci.id
) <= 3;


SELECT COUNT(*), category_id
FROM clash ci
WHERE STATUS = 1
group by category_id
having count(*) > 5;


SELECT COUNT(*)
FROM clash ci
WHERE STATUS = 1
group by category_id