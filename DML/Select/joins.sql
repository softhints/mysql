-- mysql join simple example for beginners
-- https://blog.softhints.com/mysql-join-simple-example-for-beginners/


-- (INNER) JOIN Get all couples
SELECT *
FROM aboys A
INNER JOIN agirls B
ON A.relation = B.id

SELECT A.name, B.name
FROM aboys A
INNER JOIN agirls B
ON A.relation = B.id

-- Left (OUTER) JOIN Get all boys with related girls
SELECT A.name, B.name
FROM aboys A
LEFT OUTER JOIN agirls B
ON A.relation = B.id


-- Left Excluding JOIN Get all boys without a relation
SELECT A.name, B.name
FROM aboys A
LEFT OUTER JOIN agirls B
ON A.relation = B.id
WHERE B.id IS NULL

-- Right (OUTER) JOIN Get all girls with related boys
SELECT A.name, B.name
FROM aboys A
Right OUTER JOIN agirls B
ON A.relation = B.id

-- FULL (OUTER) JOIN Return all rows
SELECT *
FROM aboys A
FULL OUTER JOIN agirls B
ON A.relation = B.id

-- simulated mysql full outer join
SELECT A.name, B.name
FROM aboys A
LEFT OUTER JOIN agirls B
ON A.relation = B.id

UNION

SELECT A.name, B.name
FROM aboys A
Right OUTER JOIN agirls B
ON A.relation = B.id

-- SQL create table
CREATE TABLE `agirls` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_bin',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_bin'
ENGINE=InnoDB
AUTO_INCREMENT=6
;

CREATE TABLE `aboys` (
	`id` INT(11) NULL DEFAULT NULL,
	`name` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_bin',
	`relation` INT(11) NULL DEFAULT NULL,
	INDEX `FK_aboys_agirls` (`relation`),
	CONSTRAINT `FK_aboys_agirls` FOREIGN KEY (`relation`) REFERENCES `agirls` (`id`)
)
COLLATE='utf8_bin'
ENGINE=InnoDB
;

INSERT INTO `aboys` (`id`, `name`, `relation`) VALUES (1, 'Brady', 5);
INSERT INTO `aboys` (`id`, `name`, `relation`) VALUES (2, 'Mike', 1);
INSERT INTO `aboys` (`id`, `name`, `relation`) VALUES (3, 'Alex', 3);
INSERT INTO `aboys` (`id`, `name`, `relation`) VALUES (4, 'Daniel', NULL);
INSERT INTO `aboys` (`id`, `name`, `relation`) VALUES (5, 'James', NULL);

INSERT INTO `agirls` (`id`, `name`) VALUES (1, 'Emma');
INSERT INTO `agirls` (`id`, `name`) VALUES (2, 'Ann');
INSERT INTO `agirls` (`id`, `name`) VALUES (3, 'Kim');
INSERT INTO `agirls` (`id`, `name`) VALUES (4, 'Olivia');
INSERT INTO `agirls` (`id`, `name`) VALUES (5, 'Victoria');

