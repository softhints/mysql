Data Definition Language (DDL) statements are used to define the database structure or schema. Some examples:

 1. CREATE - to create objects in the database
 2. ALTER - alters the structure of the database
 3. DROP - delete objects from the database
 4. TRUNCATE - remove all records from a table, including all spaces allocated for the records are removed
 5. COMMENT - add comments to the data dictionary
 6. RENAME - rename an object
